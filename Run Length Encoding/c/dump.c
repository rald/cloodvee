#include <stdio.h>
#include <ctype.h>

int main() {
	int p,c,i,d;
	char t[16];
	p=0;
	i=0;
	d=0;
	t[0]='\0';
	for(;;) {
		c=getchar();
		if(c==EOF || i==16) d=1;
		if(d==1) { 
			for(int j=0;j<16-i;j++) {
				printf("   ");
			}
			printf(" %s",t); 
			i=0; 
			d=0;
			t[0]='\0';
		}
		if(c==EOF) break;
		if(p%16==0) printf("\n%04X  ",p);
		printf("%02X ",c);
		if(i==7) printf(" ");

		if(isprint(c)) {
			t[i]=c;
		} else {
			t[i]='.';
		}

		i++;
		p++;
	}
	printf("\n");
	return 0;
}
