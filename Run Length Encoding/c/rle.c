#include <stdio.h>
int main() {
	int c1,c2,n;
	c1=getchar();
	while(c1!=EOF) {
		n=0;
		while((c2=getchar())!=EOF && c2==c1 && n<255) n++;
		putchar(n);
		putchar(c1);
		c1=c2;
	}
	return 0;
}
