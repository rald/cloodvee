# Run Length Encoding in C Language



## Files
```txt
rle.c  -> rle encoder  
rld.c  -> rle decoder  
dump.c -> hex dump  
```



## Compile
```sh
gcc rle.c -o rle  
gcc rld.c -o rld  
gcc dump.c -o dump  
```



## Run
```sh
./rle < test.txt > test.rle  # encode  
./rld < test.rle > test.rld  # decode  
./dump < test.rle  # dump encoded  
./dump < test.rld  # dump decoded  
```
